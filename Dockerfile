FROM alpine:latest
ARG RKE_VERSION

RUN apk add --no-cache curl python3 py-pip bash openssl && \
    pip3 install j2cli awscli && \
    curl -fsSL -o get_rke.sh https://gitlab.com/cmmarslender/get-rke/-/raw/master/get-rke.sh && \
    bash get_rke.sh --version $RKE_VERSION