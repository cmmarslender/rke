# RKE Image

Docker image with RKE preinstalled. Supports both amd64 and arm64

Alpine Based:

`registry.gitlab.com/cmmarslender/rke:$RKE_VERSION`

Debian Based:

`registry.gitlab.com/cmmarslender/rke:$RKE_VERSION-debian`

Available tags can be [refereced here](https://gitlab.com/cmmarslender/rke/container_registry)
